import { useEffect, useState, useCallback } from "react";
import { Dimensions, Platform, StatusBar } from "react-native";

const decorateHeights = Platform.OS === "android" ? StatusBar.currentHeight : 0;

const { width } = Dimensions.get("window");
const height = Dimensions.get("window").height - decorateHeights;

export default () => {
  const [dimensions, setDimensions] = useState({ width, height });

  const onChange = useCallback(({ window }) => {
    setDimensions({
      width: window.width,
      height: window.height - decorateHeights,
    });
  });

  useEffect(() => {
    Dimensions.addEventListener("change", onChange);

    return () => Dimensions.removeEventListener("change", onChange);
  }, []);

  return dimensions;
};

export { width, height };
