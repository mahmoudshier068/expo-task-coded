import { ApolloClient, InMemoryCache, createHttpLink } from "@apollo/client";
import { setContext } from "@apollo/client/link/context";

import { BASE_PATH } from "../Config";

let _client;

export async function getApolloClient() {
  if (_client) {
    return _client;
  }
  const cache = new InMemoryCache();

  const httpLink = createHttpLink({
    uri: `${BASE_PATH}/graphql`,
  });

  const authLink = setContext(async (_, { headers }) => {
    // get the authentication token from local storage if it exists

    // return the headers to the context so httpLink can read them
    return {
      headers: {
        ...headers,
      },
    };
  });

  const client = new ApolloClient({
    link: authLink.concat(httpLink),
    cache,
  });

  _client = client;

  return client;
}
