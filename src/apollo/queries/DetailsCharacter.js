import { gql } from "@apollo/client";
export const DETAILS_CHARACTER = gql`
  query CharactersByIds($ids: [ID!]!) {
    charactersByIds(ids: $ids) {
      id
      name
      status
      species
      type
      gender
      image
      created
      origin {
        name
      }
      location {
        name
      }
      episode {
        episode
      }
    }
  }
`;
