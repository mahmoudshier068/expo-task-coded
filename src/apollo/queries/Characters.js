import { gql } from "@apollo/client";
export const CHARACTERS = gql`
  query Query($page: Int) {
    characters(page: $page) {
      info {
        count
        pages
        next
        prev
      }
      results {
        image
        name
        id
      }
    }
  }
`;
