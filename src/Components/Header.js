import * as React from "react";
import { Pressable, Text, View } from "react-native";
import {
  moderateScale,
  responsiveFontSize,
  responsiveHeight,
} from "../UI/responsiveDimensions";
import Icon from "react-native-vector-icons/Ionicons";
import { useNavigation } from "@react-navigation/core";

const Header = (props) => {
  let { showBack, title } = props;
  const navigation = useNavigation();

  return (
    <View
      style={{
        alignSelf: "stretch",
        backgroundColor: "#15253e",
        height: responsiveHeight(8),
        justifyContent: "space-between",
        paddingHorizontal: moderateScale(5),
        flexDirection: "row",
      }}
    >
      {showBack ? (
        <Pressable
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            height: responsiveHeight(6),
            width: responsiveHeight(6),
            borderRadius: responsiveHeight(3),
            // backgroundColor:'red',
            alignSelf: "center",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Icon name="arrow-back" size={30} color="white" />
        </Pressable>
      ) : (
        <View
          style={{
            height: responsiveHeight(6),
            width: responsiveHeight(6),
          }}
        />
      )}

      <Text
        style={{
          color: "white",
          alignSelf: "center",
          fontSize: responsiveFontSize(10),
        }}
      >
        {title}
      </Text>
      <View
        style={{
          height: responsiveHeight(6),
          width: responsiveHeight(6),
        }}
      />
    </View>
  );
};
export default Header;
