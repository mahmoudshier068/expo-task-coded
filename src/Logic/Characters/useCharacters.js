import { useEffect, useState } from "react";
import { useNavigation } from "@react-navigation/native";
import { useLazyQuery, useQuery } from "@apollo/client";
import { CHARACTERS } from "../../apollo/queries/Characters";

const useCharacters = () => {
  const navigation = useNavigation();

  const [loading, setLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);
  const [data, setData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);

  const [getCharacters] = useLazyQuery(CHARACTERS, {
    fetchPolicy: "no-cache",
    onCompleted: (responseData) => {
      if (!currentPage) setCurrentPage(1);
      setData([...data, ...responseData?.characters.results]);
      setTotalPages(responseData?.characters.info.pages);
    },
    onError: (error) => {
      console.log(error);
      setRefreshing(false);
      setLoading(false);
    },
  });

  useEffect(() => {
    if (data) {
      setLoading(false);
      setRefreshing(false);
    }
  }, [data]);

  useEffect(() => {
    setLoading(true);

    getCharacters({
      variables: {
        page: currentPage ? currentPage : 1,
      },
    });
  }, [currentPage]);
  const onRefresh = () => {
    setRefreshing(true);
    setData([]);
    setCurrentPage(null);
  };

  const onEndReached = () => {
    if (totalPages >= currentPage) setCurrentPage(currentPage + 1);
  };
  const navigateTo = (screenname, params) => {
    navigation.navigate(screenname, { ...params });
  };

  return {
    onRefresh,
    loading,
    refreshing,
    navigateTo,
    onEndReached,
    data,
  };
};

export default useCharacters;
