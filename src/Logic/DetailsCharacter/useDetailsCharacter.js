import { useEffect, useState } from "react";
import { useLazyQuery, useQuery } from "@apollo/client";
import { DETAILS_CHARACTER } from "../../apollo/queries/DetailsCharacter";

const useDetailsCharacter = () => {
  const [loading, setLoading] = useState(true);
  const [detailsCharacter, setDetailsCharacter] = useState(null);
  const [id, setId] = useState(null);

  const [getDetailsChapter] = useLazyQuery(DETAILS_CHARACTER, {
    fetchPolicy: "no-cache",
    onCompleted: (responseData) => {
      setLoading(false);
      setDetailsCharacter(responseData.charactersByIds[0]);
    },
    onError: (error) => {
      setLoading(false);
      console.log(error);
    },
  });

  useEffect(() => {
    setLoading(true);
    getDetailsChapter({
      variables: {
        ids: [id],
      },
    });
  }, [id]);

  return {
    loading,
    detailsCharacter,
    setId,
  };
};

export default useDetailsCharacter;
