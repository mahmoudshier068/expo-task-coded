import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Characters from "../Screens/Characters/Characters";
import DetailsCharacter from "../Screens/DetailsCharacter/DetailsCharacter";
import * as Linking from "expo-linking";

const Stack = createStackNavigator();
const prefix = Linking.makeUrl("/");

export default Routs = () => {
  const linking = {
    prefixes: [prefix],
    config: {
      screens: {
        DetailsCharacter: {
          screen: "DetailsCharacter",
          path: "DetailsCharacter/:id",
          parse: { id: (id) => `${id}` },
        },
        Characters: "Characters",
      },
    },
  };
  return (
    <NavigationContainer linking={linking}>
      <Stack.Navigator
        headerMode="none"
        screenOptions={{
          headerShown: false,
          animationEnabled: Platform.OS === "ios",
        }}
      >
        <Stack.Screen
          name="Characters"
          component={Characters}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="DetailsCharacter"
          component={DetailsCharacter}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
