import React, { useEffect } from "react";
import {
  View,
  Text,
  ActivityIndicator,
  Image,
  ScrollView,
  StyleSheet,
} from "react-native";
import Header from "../../Components/Header";
import useDetailsCharacter from "../../Logic/DetailsCharacter/useDetailsCharacter";
import {
  moderateScale,
  responsiveFontSize,
  responsiveHeight,
  responsiveWidth,
} from "../../UI/responsiveDimensions";

const DetailsCharacter = (props) => {
  let { id, name } = props.route.params;
  let { loading, detailsCharacter, setId } = useDetailsCharacter();
  useEffect(() => {
    setId(id);
  }, []);

  const renderRow = (key, value) => {
    return (
      <View style={styles.containerRow}>
        <Text style={{ fontSize: responsiveFontSize(10) }}>{key}</Text>

        <Text>{value}</Text>
      </View>
    );
  };

  return (
    <View style={{ flex: 1 }}>
      <Header title={name} showBack />
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        {detailsCharacter ? (
          <View style={{ flex: 1 }}>
            <Image
              style={styles.image}
              resizeMode="contain"
              source={{ uri: detailsCharacter.image }}
            />
            <ScrollView>
              {renderRow("name", detailsCharacter.name)}
              {renderRow("gender", detailsCharacter.gender)}
              {renderRow("origin", detailsCharacter.origin.name)}
              {renderRow("location", detailsCharacter.location.name)}
              {renderRow("status", detailsCharacter.status)}
              {renderRow("species", detailsCharacter.species)}
              {renderRow("type", detailsCharacter.type)}
              {renderRow(
                "episode",
                detailsCharacter.episode.length + " episode"
              )}
            </ScrollView>
          </View>
        ) : (
          <View>
            <ActivityIndicator size="large" color="#15253e" />
          </View>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  containerRow: {
    flexDirection: "row",
    borderColor: "grey",
    borderBottomWidth: moderateScale(0.2),
    marginVertical: moderateScale(3),
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: moderateScale(20),
    paddingBottom: moderateScale(2),
  },
  image: {
    alignSelf: "stretch",
    borderTopLeftRadius: moderateScale(5),
    borderTopRightRadius: moderateScale(5),
    height: responsiveHeight(25),
    width: responsiveWidth(100),
  },
});
export default DetailsCharacter;
