import * as React from "react";
import { View, Text, Pressable, Image, StyleSheet } from "react-native";
import useCharacters from "../../Logic/Characters/useCharacters";
import {
  moderateScale,
  responsiveFontSize,
  responsiveHeight,
  responsiveWidth,
} from "../../UI/responsiveDimensions";

const Row = (props) => {
  let { item } = props;
  const { navigateTo } = useCharacters();

  return (
    <Pressable
      android_ripple
      onPress={() => {
        navigateTo("DetailsCharacter", { ...item });
      }}
      style={styles.container}
    >
      <Image style={styles.image} source={{ uri: item.image }} />
      <Text style={styles.title} numberOfLines={1}>
        {item.name}
      </Text>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: moderateScale(5),
    borderWidth: moderateScale(0.1),
    backgroundColor: "white",
    alignSelf: "center",
    justifyContent: "center",
    alignSelf: "stretch",
    alignItems: "center",
    elevation: 1,
    margin: moderateScale(5),
    flex: 1,
  },
  image: {
    alignSelf: "stretch",
    borderTopLeftRadius: moderateScale(5),
    borderTopRightRadius: moderateScale(5),
    height: responsiveHeight(25),
  },
  title: {
    marginVertical: moderateScale(5),
    fontSize: responsiveFontSize(9),
  },
});

export default Row;
