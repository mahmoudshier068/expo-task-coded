import * as React from "react";
import {
  View,
  Text,
  RefreshControl,
  FlatList,
  ActivityIndicator,
} from "react-native";
// import { FlatList } from "react-native-gesture-handler";
import Header from "../../Components/Header";
import useCharacters from "../../Logic/Characters/useCharacters";
import Row from "./Row";

const Characters = () => {
  const { onRefresh, loading, onEndReached, refreshing, navigateTo, data } =
    useCharacters();

  return (
    <View style={{ flex: 1 }}>
      <Header title="Characters" />
      <FlatList
        renderItem={({ item, index }) => {
          return <Row item={item} key={index} />;
        }}
        refreshControl={
          <RefreshControl
            colors={["#15253e", "red", "black", "blue", "orange", "#152e3e"]}
            onRefresh={onRefresh}
            enabled={true}
            refreshing={refreshing}
          />
        }
        onEndReachedThreshold={0.8}
        onEndReached={onEndReached}
        ListFooterComponent={loading?<ActivityIndicator size="large" color="#15253e" />:""}
        numColumns={2}
        keyExtractor={(item) => item.id}
        data={data}
      />
    </View>
  );
};
export default Characters;
