# Expo Task coded Q2 🙋🏻‍♂️🙋🏻‍♂️🙋🏻‍♂️



## Second
- these third party libraries will help me with this issue  
     - react-navigation. & react-navigation/bottom-bar   
            use this library to navigate between screens and make a fixed bottom bar 
     - redux & redux-thunk & react-redux  
            to hundel all login in global state to add product to cart by useing useDispatch() and useSelector() to show all content from global state to cart page 
     - apollo client. & Axios 
            if requests with HTTP request will use Axios if it graphql will use apollo client 

## Step (1) 
    will create all screens that i have (cart-products-home-profile-productDetails...)
## Step (2)
    hundel fetch this request with Axios to get all products that i will have request :  
                url https://localhost/products
                response : {
                    data:[],
                    totalPages: , 
                    currentPage: , 
                    next : ,
                    prev : , 
                }
## Step (3) 
    hundel fetch this request with Axios to get details info product that pressed on it:  
                url https://localhost/products/:id
                response : {
                    images:[],
                    name: , 
                    price: , 
                    description : ,
                    ...
                }

## Step (4)
    hundel redux and global state all reducers (cart,user....)
    reducerCart :{
        allProducts: [
            {productId: , qty : }
        ], 
        totalCost : 
        guestCartId :   => if the user is not logged in and there is a request for a cart this helps analytics to see all reports 
    }
    build all function that help me to control every thing in cart like 
         * add product  
            use useDispatch() with specific type to control array in reducers  
         * increase/decrease count 
            use useDispatch() with specific type to control array in reducers and update number of count of this product 
         * delete product
            use useDispatch() & filter function to delete index from array and refect in my UI  
         * delete allProducts 
            just use useDispatch() and return intial state with specific type of action 



## important 

    the cart of products and these operations already required from me in other tasks. You can check this from this link 
https://gitlab.com/mahmoudshier068/expo-task/-/tree/master