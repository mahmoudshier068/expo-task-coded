import { ApolloProvider } from "@apollo/client";
import React, { useState, useEffect } from "react";
import { View, Text, StatusBar, SafeAreaView, LogBox } from "react-native";
import { getApolloClient } from "./src/apollo/client";

import Routs from "./src/Navigation/Routs";

const App = () => {
  const [client, setClient] = useState(null);

  useEffect(() => {
    LogBox.ignoreAllLogs();
    getApolloClient().then(setClient).catch();
  }, []);

  if (client)
    return (
      <ApolloProvider client={client}>
        <SafeAreaView style={[{ flex: 1, alignSelf: "stretch" }]}>
          <StatusBar barStyle="light-content" backgroundColor="#15253e" />
          <Routs />
        </SafeAreaView>
      </ApolloProvider>
    );
  else {
    return <View></View>;
  }
};

export default App;
